$(window).on("load", function() {
    "use strict";

    $('.video').magnificPopup({
        disableOn: 700,
        type: 'iframe',
        mainClass: 'mfp-fade',
        removalDelay: 160,
        preloader: false,
        fixedContentPos: false
          
    });
});
//videocarousel


// dropdown
$('ul.nav li.dropdown').hover(function() {
  $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeIn(500);
}, function() {
  $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeOut(500);
});
// responsive
$(window).resize(function(e) {
    if ($(window).width() <= 1024) {
        $('.sub-menu-parent').click(function() {
          $(this).find('ul.sub-menu').toggle();
            e.preventDefault();
        });
    }
}).resize();     

// small nav
function init() {
     var navbarHeight = $("nav").outerHeight() 
     if ($(window).width() > 960) {
          window.addEventListener('scroll', function(e){
             var distanceY = window.pageYOffset || document.documentElement.scrollTop,
                 shrinkOn = 00,
                 header = document.querySelector(".navbar");
             if (distanceY > shrinkOn) {
                 classie.add(header,"smaller");
                 $("#main").css('marginTop',navbarHeight);
             } else {
                 if (classie.has(header,"smaller")) {
                     classie.remove(header,"smaller");
                    $("#main").css('marginTop',0);

                 }
             }
          });
     }
}
window.onload = init();

// scroll-up
$(window).scroll(function () {
    if ($(this).scrollTop() > 150) {
        $('.scrollup').fadeIn();
    } else {
        $('.scrollup').fadeOut();
    }
});

$('.scrollup').click(function () {
    $("html, body").animate({
        scrollTop: 0
    }, 600);
    return false;
});

// collapse
$('.collapse').on('shown.bs.collapse',function(){
    $(this).parent().find(".accordin-icon").removeClass("accordin-icon-plus");
    $(this).siblings('.panel-heading').find('.accordion-toggle').addClass('red');
    $(this).siblings('.panel-heading').addClass('opened');});
$('.collapse').on('hidden.bs.collapse',function(){       
    $(this).parent().find(".accordin-icon").addClass("accordin-icon-plus");
    $(this).siblings('.panel-heading').find('.accordion-toggle').removeClass('red');        
    $(this).siblings('.panel-heading').removeClass('opened');
});
// showmore
$(".showmore").click(function() {
    var that = $(this);
    $(this).parent().find('.moretext').slideToggle(500,"linear",function(e) {
        var text = that.html();
        var is_visible = $(this).is(':visible');
        if (is_visible) {
            that.html('Show Less');
        } else {
            that.html('Read More');
        }
    });
});

// magnific popup gallery
jQuery(document).ready(function() {
        jQuery('.documentimages').magnificPopup({
            delegate: 'a',
            type: 'image',
        callbacks: {
          elementParse: function(item) {
            // Function will fire for each target element
            // "item.el" is a target DOM element (if present)
            // "item.src" is a source that you may modify
            console.log(item.el.context.className);
            if(item.el.context.className == 'video') {
              item.type = 'iframe',
              item.iframe = {
                 patterns: {
                   youtube: {
                     index: 'youtube.com/', // String that detects type of video (in this case YouTube). Simply via url.indexOf(index).

                     id: 'v=', // String that splits URL in a two parts, second part should be %id%
                      // Or null - full URL will be returned
                      // Or a function that should return %id%, for example:
                      // id: function(url) { return 'parsed id'; } 

                     src: '//www.youtube.com/embed/%id%?autoplay=1' // URL that will be set as a source for iframe. 
                   },
                   vimeo: {
                     index: 'vimeo.com/',
                     id: '/',
                     src: '//player.vimeo.com/video/%id%?autoplay=1'
                   },
                   gmaps: {
                     index: '//maps.google.',
                     src: '%id%&output=embed'
                   }
                 }
              }
            } else {
               item.type = 'image',
               item.tLoading = 'Loading image #%curr%...',
               item.mainClass = 'mfp-img-mobile',
               item.image = {
                 tError: '<a href="%url%">The image #%curr%</a> could not be loaded.'
               }
            }

          }
        },
            gallery: {
                enabled: true,
                navigateByImgClick: true,
                preload: [0,1] // Will preload 0 - before current, and 1 after the current image
            }
        
        });
  
    });

// for share trip
// $("#shareit").click( function() {
//     $(".btn-mask").show();
//     if ($(".btn-mask").is(":visible")) {
//         $(".fa-times").show();
//         $("#shareit").hide();
//         $('.btn-back').addClass('transform');
//         $('.share-content').show();
//     }
// });

// $(".fa-times").click( function() {
//     $("#shareit").show();
//     $(".fa-times").hide();
//     $('.btn-back').removeClass('transform');
//     $('.share-content').hide( function() {
//         $(".btn-mask").hide();
//     });
// });
